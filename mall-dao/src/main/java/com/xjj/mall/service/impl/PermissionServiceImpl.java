/****************************************************
 * Description: ServiceImpl for t_mall_permission
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.PermissionEntity;
import com.xjj.mall.dao.PermissionDao;
import com.xjj.mall.service.PermissionService;

@Service
public class PermissionServiceImpl extends XjjServiceSupport<PermissionEntity> implements PermissionService {

	@Autowired
	private PermissionDao permissionDao;

	@Override
	public XjjDAO<PermissionEntity> getDao() {
		
		return permissionDao;
	}
}